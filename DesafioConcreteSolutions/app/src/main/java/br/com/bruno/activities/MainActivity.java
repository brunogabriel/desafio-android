package br.com.bruno.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.List;

import br.com.bruno.R;
import br.com.bruno.adapter.ShotAdapter;
import br.com.bruno.model.Shot;
import br.com.bruno.model.ShotAnswer;
import br.com.bruno.util.SingletonElement;
import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.functions.Action1;

/**
 * Created by bruno on 6/23/15.
 */
public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.recyclerview)
    protected RecyclerView recyclerView;

    protected ShotAdapter shotAdapter;

    private LinearLayoutManager layoutManager;

    // Pagination
    boolean isLoading = true;
    private int nextPage = 1;
    private int totalPage = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        startRecyclerView();
        makeRequest();
    }

    // Start Recycler Components
    private void startRecyclerView() {
        // Layout Manager
        layoutManager = new LinearLayoutManager(this);

        // RecyclerView
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading && layoutManager.findLastVisibleItemPosition()+1 >=layoutManager.getItemCount()) {
                    isLoading = true;
                    makeRequest();
                }
            }
        });
        // Adapter
        shotAdapter = new ShotAdapter(this);
        recyclerView.setAdapter(shotAdapter);
    }

    private void makeRequest() {
        SingletonElement.getInstance().getRestAPI().getPopularShots(nextPage).subscribe(onSuccess, onError);
    }

    // Actions Request
    protected Action1<ShotAnswer> onSuccess = shotAnswer -> {
        if(totalPage==-1) {
            totalPage = shotAnswer.getTotal();
        }
        List<Shot> shotList = shotAnswer.getShots();
        try {
            if(nextPage<totalPage) {
                shotAdapter.addItems(shotList, true);
                nextPage++;
                isLoading = false;
            } else {
                shotAdapter.addItems(shotList, false);
                isLoading = true;
            }
        } catch (Exception e) {
            Snackbar.make(findViewById(android.R.id.content), R.string.app_error_loading,
                    Snackbar.LENGTH_LONG).show();
            shotAdapter.removeLoader();
            isLoading = true;
        }
    };

    protected Action1<Throwable> onError = throwable -> {
        Snackbar.make(findViewById(android.R.id.content), R.string.app_error_loading,
                Snackbar.LENGTH_LONG).show();
        shotAdapter.removeLoader();
        isLoading = true;
    };
}
