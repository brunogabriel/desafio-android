package br.com.bruno.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bruno on 6/29/15.
 */
public class Player {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("avatar_url")
    private String avatarURL;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatar_url(String avatarURL) {
        this.avatarURL = avatarURL;
    }

}