package br.com.bruno.activities;


import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

import br.com.bruno.R;
import br.com.bruno.adapter.ShotAdapter;
import br.com.bruno.model.Player;
import br.com.bruno.model.Shot;
import br.com.bruno.model.ShotDetail;
import br.com.bruno.util.SingletonElement;
import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.functions.Action1;


/**
 * Created by bruno on 6/25/15.
 */
public class DetailActivity extends AppCompatActivity {

    Shot shot;

    @InjectView(R.id.shotImage)
    protected SimpleDraweeView shotImage;

    @InjectView(R.id.toolbar)
    protected Toolbar toolbar;

    @InjectView(R.id.eyeText)
    protected TextView eyeText;

    @InjectView(R.id.mindText)
    protected TextView mindText;

    @InjectView(R.id.heartText)
    protected TextView heartText;

    // Details
    @InjectView(R.id.shotTitle)
    protected TextView shotTitle;

    @InjectView(R.id.avatarImage)
    SimpleDraweeView avatarImage;

    @InjectView(R.id.usernameText)
    protected TextView usernameText;

    @InjectView(R.id.description)
    protected TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fresco.initialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.inject(this);

        // Toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle("");

        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable(ShotAdapter.SHOT_PARCEL) != null) {
            shot = (Shot) getIntent().getExtras().getParcelable(ShotAdapter.SHOT_PARCEL);
            loadImage();
            shotTitle.setText(shot.getTitle());
            // Chamada de API
            SingletonElement.getInstance().getRestAPI().getDetails(shot.getId()).subscribe(onSuccess, onError);

        }
    }


    private void loadImage() {
        // Verifica se existe a menor imagem possivel
        String imageURL = shot.getImage400URL();
        if (imageURL == null || imageURL.isEmpty()) {
            imageURL = shot.getImageURL();
        }

        Uri uri = Uri.parse(imageURL);
        DraweeController controller = Fresco.newDraweeControllerBuilder().
                setUri(uri).setAutoPlayAnimations(true).build();
        shotImage.setController(controller);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return true;
    }

    protected Action1<ShotDetail> onSuccess = shotDetail -> {
        Log.d("Retrofit", "onSuccess");
        if(shotDetail!=null) {
            runOnUiThread(()->{
                eyeText.setText("" + shot.getViewsCount());
                mindText.setText("" + shotDetail.getCommentsCount());
                heartText.setText("" + shotDetail.getLikesCount());
                try {
                    description.setText("" + Html.fromHtml(shotDetail.getDescription()));
                } catch (Exception e) {

                }

                Player player = shotDetail.getPlayer();

                if(player!=null) {
                    usernameText.setText(player.getName());
                    avatarImage.setImageURI(Uri.parse(player.getAvatarURL()));
                 }
            });

        }
    };

    protected Action1<Throwable> onError = throwable -> {
        Log.d("Retrofit", "onError: " + throwable.getMessage());
        Snackbar.make(findViewById(android.R.id.content), R.string.app_connection_error,
                Snackbar.LENGTH_LONG).show();
    };


}
