package br.com.bruno.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bruno on 6/29/15.
 */
public class ShotDetail {

    @SerializedName("likes_count")
    private Integer likesCount;

    @SerializedName("comments_count")
    private Integer commentsCount;

    @SerializedName("description")
    private String description;

    @SerializedName("player")
    private Player player;

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
