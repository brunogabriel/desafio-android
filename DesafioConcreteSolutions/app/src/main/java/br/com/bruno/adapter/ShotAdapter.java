package br.com.bruno.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.List;
import br.com.bruno.R;
import br.com.bruno.activities.DetailActivity;
import br.com.bruno.activities.MainActivity;
import br.com.bruno.holders.LoaderHolder;
import br.com.bruno.model.AppLoader;
import br.com.bruno.model.Shot;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bruno on 6/23/15.
 */
public class ShotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String SHOT_PARCEL = "com.parcel.shot";

    public static final int BODY = 0;
    public static final int FOOTER = 1;

    private MainActivity mainActivity;
    private List<Object> items = new ArrayList<>();

    public ShotAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;;
        addLoader();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = null;
        if(viewType == FOOTER) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_loading, viewGroup, false);
            return new LoaderHolder(view, mainActivity);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.holder_shot, viewGroup, false);
            return new ShotHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ShotHolder) {
            try {
                ShotHolder shotHolder = (ShotHolder) viewHolder;
                Shot shot = (Shot) items.get(position);

                // Aplica a quantidade de views
                shotHolder.eyeText.setText("" + shot.getViewsCount());
                // Titulo
                shotHolder.titleText.setText("" + shot.getTitle());

                // Verifica se existe a menor imagem possivel
                String imageURL = shot.getImage400URL();
                if(imageURL==null || imageURL.isEmpty()) {
                    imageURL = shot.getImageURL();
                }

                Uri uri = Uri.parse(imageURL);
                DraweeController controller = Fresco.newDraweeControllerBuilder().
                        setUri(uri).setAutoPlayAnimations(true).build();
                shotHolder.shotImage.setController(controller);
                YoYo.with(Techniques.Tada).duration(1000).playOn(shotHolder.itemView);

            } catch (Exception e) {
                Log.d("ExceptionOn", "onBindViewHolder");
            }
        } else {

            LoaderHolder loaderHolder = (LoaderHolder) viewHolder;

            try {
                YoYo.with(Techniques.StandUp).duration(1000).playOn(loaderHolder.getItemView());
            } catch (Exception e) {

            }

        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) instanceof AppLoader ? FOOTER: BODY;
    }

    /**
     *
     * @param shots list of shots
     * @param isAddLoader if we need to add a new loader
     */
    public void addItems (List<Shot> shots, boolean isAddLoader) {
        // First remove loader
        removeLoader();

        // Apply and add new items
        int firstPosition = items.size();
        items.addAll(shots);
        if(isAddLoader) {
            items.add(new AppLoader());
        }
        notifyItemRangeInserted(firstPosition, isAddLoader? shots.size()+1: shots.size());
    }

    public boolean isLastItemLoader() {
        try {
            return items.get(items.size()-1) instanceof AppLoader;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Function that will to remove last item if it is a loading
     */
    public void removeLoader() {
        if(isLastItemLoader()) {
            int lastPosition = items.size()-1;
            items.remove(lastPosition);
            notifyItemRemoved(lastPosition);
        }
    }

    public void addLoader() {
        if (!isLastItemLoader()) {
            int position = items.size();
            items.add(new AppLoader());
            notifyItemInserted(position);
        }
    }

    public class ShotHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @InjectView(R.id.shotImage)
        protected SimpleDraweeView shotImage;

        @InjectView(R.id.eyeText)
        protected TextView eyeText;

        @InjectView(R.id.titleText)
        protected TextView titleText;

        View itemView;

        public ShotHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.inject(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent mIntent = new Intent(mainActivity, DetailActivity.class);
            mIntent.putExtra(SHOT_PARCEL, (Shot) items.get(getAdapterPosition()));
            mainActivity.startActivity(mIntent);
        }
    }

}
