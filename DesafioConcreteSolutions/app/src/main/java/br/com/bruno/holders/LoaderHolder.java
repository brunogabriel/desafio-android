package br.com.bruno.holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import br.com.bruno.R;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by bruno on 6/23/15.
 */
public class LoaderHolder extends RecyclerView.ViewHolder {

    @InjectView(R.id.imageLoading)
    protected ImageView imageLoading;

    protected View itemView;

    private Activity activity;


    public LoaderHolder(View itemView, Activity activity) {
        super(itemView);
        this.itemView = itemView;
        this.activity = activity;
        ButterKnife.inject(this, itemView);
    }

    public View getItemView() {
        return itemView;
    }


}
