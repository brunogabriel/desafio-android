package br.com.bruno.util;

import br.com.bruno.network.RestAPI;
import retrofit.RestAdapter;

/**
 * Created by bruno on 6/29/15.
 */
public class SingletonElement {

    public final String BASE_URL = "http://api.dribbble.com";

    private static SingletonElement instance;


    // RestAdapter
    private RestAdapter restAdapter =
            new RestAdapter.Builder().setEndpoint(BASE_URL).build();
    private RestAPI restAPI =
            restAdapter.create(RestAPI.class);

    private SingletonElement() {

    }

    public static SingletonElement getInstance() {
        if(instance == null) {
            instance = new SingletonElement();
        }
        return instance;
    }

    public RestAPI getRestAPI() {
        return restAPI;
    }
}
