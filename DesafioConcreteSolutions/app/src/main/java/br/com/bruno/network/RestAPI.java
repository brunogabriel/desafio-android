package br.com.bruno.network;

import br.com.bruno.model.ShotAnswer;
import br.com.bruno.model.ShotDetail;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by bruno on 6/23/15.
 */
public interface RestAPI {

    @GET("/shots/popular")
    Observable<ShotAnswer> getPopularShots(@Query("page") int page);

    @GET("/shots/{shot_id}")
    Observable<ShotDetail> getDetails(@Path("shot_id") Integer shotId);
}
